# Marketplace usando Stripe Connect

Este ejemplo muestra cómo crear un marketplace temático de alquiler de viviendas utilizando [Stripe Payments](http://stripe.com/payments) y [Stripe Connect](https://stripe.com/connect) donde los clientes pueden registrarse y convertirse en arrendatarios (compradores) y propietarios (vendedores).

Debajo de la superficie, se proporciona el genérico del marketplace con la noción de `buyers`, `sellers` y `transactions` para los bienes vendidos y los flujos de trabajo básicos que muestran cómo usar Pagos y Conectar juntos.

Las vendedores están a bordo usando [Stripe Connect Express](https://stripe.com/connect/express) que les da a los vendedores una cuenta de Stripe conectada al marketplace. Cuando el marketplace acepta fondos a través de pagos con tarjeta, los fondos se envían a las cuentas de Stripe del vendedor como parte de cada transacción del marketplace.


<img src="./demo.png" alt="Preview of recipe" align="center">

## Caracteristicas

- Los usuarios pueden registrarse y, opcionalmente, decidir convertirse en vendedores al incorporarse a Stripe.
- Los listados se pueden enumerar, mostrar, agregar, editar y eliminar.
- Las reservas se pueden ver en detalle como transacciones.
- El marketplace acepta pagos con tarjeta y los fondos se envían a la cuenta de Stripe conectada del vendedor.
- Los usuarios pueden visitar su tablero para obtener una descripción general de sus listados, transacciones y saldos de cuenta.
- Página de administración simple para el servicio de limpieza.

## Arquitectura

El marketplace se implementa como una aplicación de FullStack impulsada por [Next.js](https://nextjs.org/) y contiene un front-end de React y una API REST de Node.js.

La App renderiza sus componentes React como servidor y cliente usando [isomorphic rendering](https://matwrites.com/universal-react-apps-start-with-next-js/).

![](https://matwrites.com/wp-content/uploads/2017/06/Isomorphic-web-apps.png)

### Caracteristicas Tecnicas

- Sistema de autenticacion usando [JWT tokens](https://jwt.io/) con paginas de login y sign up.
- Almacenamiento usando [LowDB](https://github.com/typicode/lowdb) que provee una basica base de datos local en JSON.
- REST API scaffolding con endpoints de autenticacion para recursos como `listings`, `login`, `payouts`, `profile`, `signup`, `transactions`, y `users`.
- Pagos de tarjeta son aceptados via [Payment Intents API](https://stripe.com/docs/payments/payment-intents) + [Stripe Elements](https://stripe.com/payments/elements) y esta integrado con [`react-stripe-elements`](https://github.com/stripe/react-stripe-elements)

## Comenzamos

Ejecuta lo siguiente:

**1. Clona**

```
git clone https://gitlab.com/daicarjim/marketplace-model-airbnb-with-stripeconnect-test.git
```

**2. Configura Stripe**

Lo primero que debe hacer es crear una plataforma Connect. Para hacerlo, vaya a https://dashboard.stripe.com/test/connect/accounts/overview.

Una vez que haya registrado su plataforma Connect, ahora puede generar una identificación de usuario (User Id) de Connect. Necesita esto junto con sus claves API de Stripe.

**2. Obten las API keys de Stripe y configure las variables de entorno**

Vaya al [developer dashboard](https://dashboard.stripe.com/apikeys) de Stripe para encontrar sus claves API (developer settings) y su ID de usuario de Connect (Connect setttings).

Copie el archivo .env.example en un archivo llamado .env en la carpeta del servidor que desea usar. Por ejemplo:

```
cp .env.example .env
```

Ahora copie sus claves API de Stripe y la identificación de usuario de Connect en su archivo `.env`:

```
STRIPE_PUBLIC_KEY=<replace-with-your-publishable-key>
STRIPE_SECRET_KEY=<replace-with-your-secret-key>
STRIPE_CLIENT_ID=<replace-with-your-connect-client-id>
```


**3.  Establecer un URI de redireccionamiento para Stripe Onboarding**

Vaya a su [connect settings](https://dashboard.stripe.com/settings/applications) para agregar un URI de redirección para que Stripe vuelva a llamar después de que un vendedor se incorpore a Stripe.

El URI de los usuarios de esta aplicación es [http://localhost:3000/stripe/callback](http://localhost:3000/stripe/callback)

### Ejecutar App en local

1. `npm install`
1. `npm run dev`
1. Ahora está listo para usar la aplicación que se ejecuta en [http://localhost:3000](http://localhost:3000).
1. El mercado debería estar disponible, y si va a `/iniciar sesión` debería poder iniciar sesión como inquilinos y propietarios utilizando los botones de demostración.
1. Deberá incorporar al propietario a su plataforma Stripe Connect. Si inicia sesión con la demostración del propietario y va a "Listados", puede habilitar los pagos. Esto lo llevará a través de la incorporación alojada de Stripe con Connect.
1. Una vez que se complete la incorporación, puede usar la demostración del arrendatario para ver cómo funcionan los pagos.
1. 🎉

## Mas recursos y fuentes relacionadas

https://stripe.com/docs/connect#start-with-a-guide

https://stripe.com/docs/connect/collect-then-transfer-guide?platform=web#prerequisites

https://github.com/stripe/stripe-demo-connect-kavholm-marketplace

https://kavholm.com/

https://support.stripe.com/questions/best-practices-for-connect-platforms-communicating-updates-to-verification-requirements-with-custom-connected-accounts

https://support.stripe.com/questions/best-practices-for-connect-platforms-communicating-updates-to-verification-requirements-with-standard-or-express-connected-accounts


## Author(s)

[@auchenberg-stripe](https://twitter.com/auchenberg)
